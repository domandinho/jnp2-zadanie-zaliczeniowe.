package pl.edu.mimuw.students.jnp;

import java.sql.*;
import java.util.Iterator;
import java.util.Properties;
import java.util.Random;
import java.util.logging.Logger;
import javax.sql.*;

import oracle.jdbc.pool.*;

class ConnectionToDatabase {

    private static String getConnectionString(Properties properties) {
        String connectionString = "jdbc:oracle:thin:@//";
        connectionString += properties.getProperty("main.connection.host");
        connectionString += ":";
        connectionString += properties.getProperty("main.connection.port");
        connectionString += "/";
        connectionString += properties.getProperty("main.connection.databasename");
        return connectionString;
    }

    public static String connect(Properties properties, String sqlQuery) {
        try {
            OracleDataSource dataSource = new OracleDataSource();
            dataSource.setURL(getConnectionString(properties));
            Connection conn = dataSource.getConnection(
                    properties.getProperty("main.connection.user"),
                    properties.getProperty("main.connection.password"));
            PreparedStatement stmt = conn.prepareStatement(sqlQuery);
            ResultSet rset = stmt.executeQuery();
            StringBuilder builder = new StringBuilder("");
            while (rset.next()) {
                int howMany = 2;
                if (sqlQuery.equals(AwrReportSqlGenerator.getAllSnapshotsQuery())) {
                    howMany = 4;
                }
                for (int i = 1; i < howMany; i++) {
                    builder.append(rset.getString(i) + " ");
                }
                builder.append("\n");
            }
            conn.close();
            return builder.toString();
        } catch (SQLException ex) {
            return "SQLException: " + ex.getMessage();
        }
    }
}
