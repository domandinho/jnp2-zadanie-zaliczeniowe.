package pl.edu.mimuw.students.jnp;

/**
 * Created by domandinho on 09.06.15.
 */
public class AwrReportSqlGenerator {
    public final String beginSnapshotId;
    public final String endShapshotId;
    public final String dataBaseId;
    public final String dataBaseNumber;

    public static String getAllSnapshotsQuery() {
        return "select snap_id, snap_level, to_char(begin_interval_time, 'dd/mm/yy hh24:mi:ss') begin from dba_hist_snapshot order by 1";
    }

    public AwrReportSqlGenerator(String beginSnapshotId, String endShapshotId,
                                 String dataBaseId, String dataBaseNumber) {
        this.beginSnapshotId = beginSnapshotId;
        this.endShapshotId = endShapshotId;
        this.dataBaseId = dataBaseId;
        this.dataBaseNumber = dataBaseNumber;
    }

    public String getSQLQuery() {
        return "SELECT output FROM TABLE (dbms_workload_repository.awr_report_html ( " +
                dataBaseId +
                ", " +
                dataBaseNumber +
                "," +
                beginSnapshotId +
                ", " +
                endShapshotId +
                " ))";
    }
}
