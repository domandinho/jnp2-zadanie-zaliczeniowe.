package pl.edu.mimuw.students.jnp;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.ReadableByteChannel;
import java.util.Properties;
import java.util.Scanner;
import javax.swing.JFileChooser;

import org.jsoup.Jsoup;

public class MainPage extends JFrame {
    private JButton connectToDatabaseButton;
    private JPanel mainpanel;
    private JTextArea awrreportcontent;
    private JTabbedPane maintabbedpane;
    private JPanel connection;
    private JPanel settingspanel;
    private JTextField textFieldHostName;
    private JTextField textFieldPortNumber;
    private JTextField textFieldDatabaseName;
    private JTextField textFieldUsername;
    private JPasswordField passwordFieldDatabasePassword;
    private JTextField textFieldBeginSnaphotId;
    private JTextField textFieldEndShapshotId;
    private JTextField textFieldDatabaseId;
    private JTextField textFieldDatabaseNumber;
    private JButton buttonLoadAWRReportFromFile;
    private JButton buttonShowSnapshots;
    private JTextArea textAreaSnapshots;
    private Properties properties;

    private DocumentListener documentListenerUpdateProperties = new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
            updateProperties();
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
            updateProperties();
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
            updateProperties();
        }
    };

    private void setAWRContent(String text) {
        synchronized (awrreportcontent) {
            awrreportcontent.setText(text);
        }
    }

    private class AWRVisualiserRunner implements Runnable {
        @Override
        public void run() {
            connectToDatabaseButton.setEnabled(false);
            buttonLoadAWRReportFromFile.setEnabled(false);
            AwrReportSqlGenerator report = new AwrReportSqlGenerator(
                    textFieldBeginSnaphotId.getText(),
                    textFieldEndShapshotId.getText(),
                    textFieldDatabaseId.getText(),
                    textFieldDatabaseNumber.getText()
            );
            String content = ConnectionToDatabase.connect(properties,
                    report.getSQLQuery());
            JFrame frame = new AWRVisualisator("AWR", content);
            buttonLoadAWRReportFromFile.setEnabled(true);
            connectToDatabaseButton.setEnabled(true);
        }
    }

    private class SnapShotsGetter implements Runnable {
        @Override
        public void run() {
            buttonLoadAWRReportFromFile.setEnabled(false);
            String snapshots = ConnectionToDatabase.connect(properties,
                    AwrReportSqlGenerator.getAllSnapshotsQuery());
            textAreaSnapshots.setText(snapshots);
            buttonLoadAWRReportFromFile.setEnabled(true);
        }
    }

    private ActionListener connectToDB = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            Thread thread = new Thread(new AWRVisualiserRunner());
            thread.start();
        }
    };

    private ActionListener loadAWRReportFromFile = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            JFileChooser jFileChooser = new JFileChooser();
            int valueReturnedByFileChooser = jFileChooser.showOpenDialog(MainPage.this);
            if (valueReturnedByFileChooser == JFileChooser.APPROVE_OPTION) {
                try {
                    Scanner scanner = new Scanner(
                            new FileInputStream(jFileChooser.getSelectedFile()));
                    StringBuilder builder = new StringBuilder();
                    while (scanner.hasNext()) {
                        builder.append(scanner.next());
                        builder.append("\n");
                    }
                    String awrReportContent = builder.toString();
                    JFrame frame = new AWRVisualisator("AWR", awrReportContent);
                } catch (IOException ioException) {
                    setAWRContent(ioException.getMessage());
                }
            } else {
                if (valueReturnedByFileChooser == JFileChooser.CANCEL_OPTION) {
                    setAWRContent("You pressed cancel");
                }
            }
        }
    };

    private void setDefaultProperties()
            throws IOException {
        properties = new Properties();
        properties.setProperty("main.connection.host", "localhost");
        properties.setProperty("main.connection.port", "1521");
        properties.setProperty("main.connection.databasename", "oracledb");
        properties.setProperty("main.connection.user", "scott");
        properties.setProperty("main.connection.password", "tiger");
        properties.setProperty("main.awr.beginsnapshotid", "0");
        properties.setProperty("main.awr.endsnapshotid", "1");
        properties.setProperty("main.awr.databaseid", "0");
        properties.setProperty("main.awr.databasenumber", "0");
        properties.store(new FileOutputStream("./connection.properties"),
                "<!-- no comments-->");
    }

    private void setTextFieldsValuesFromConfiguration(JTextField textField, String propertyName) {
        textField.setText(properties.getProperty(propertyName));
    }

    private void insertValuesFromConfigurationIntoLabels() {
        setTextFieldsValuesFromConfiguration(textFieldHostName, "main.connection.host");
        setTextFieldsValuesFromConfiguration(textFieldPortNumber, "main.connection.port");
        setTextFieldsValuesFromConfiguration(textFieldDatabaseName, "main.connection.databasename");
        setTextFieldsValuesFromConfiguration(textFieldUsername, "main.connection.user");
        setTextFieldsValuesFromConfiguration(textFieldBeginSnaphotId, "main.awr.beginsnapshotid");
        setTextFieldsValuesFromConfiguration(textFieldEndShapshotId, "main.awr.endsnapshotid");
        setTextFieldsValuesFromConfiguration(textFieldDatabaseId, "main.awr.databaseid");
        setTextFieldsValuesFromConfiguration(textFieldDatabaseNumber, "main.awr.databasenumber");
        passwordFieldDatabasePassword.setText(properties.getProperty("main.connection.password"));
    }

    private void loadProperties() {
        try {
            properties = new Properties();
            properties.load(new FileInputStream("./connection.properties"));
        } catch (IOException e) {
            try {
                setDefaultProperties();
            } catch (IOException error) {
            }
        }
        insertValuesFromConfigurationIntoLabels();
    }

    private void assignValuesFromTextFieldsToProperties(JTextField jTextField,
                                                        String propertyName) {
        properties.setProperty(propertyName, jTextField.getText());
    }

    private void updateProperties() {
        assignValuesFromTextFieldsToProperties(textFieldHostName, "main.connection.host");
        assignValuesFromTextFieldsToProperties(textFieldPortNumber, "main.connection.port");
        assignValuesFromTextFieldsToProperties(textFieldDatabaseName, "main.connection.databasename");
        assignValuesFromTextFieldsToProperties(textFieldUsername, "main.connection.user");
        assignValuesFromTextFieldsToProperties(textFieldBeginSnaphotId, "main.awr.beginsnapshotid");
        assignValuesFromTextFieldsToProperties(textFieldEndShapshotId, "main.awr.endsnapshotid");
        assignValuesFromTextFieldsToProperties(textFieldDatabaseId, "main.awr.databaseid");
        assignValuesFromTextFieldsToProperties(textFieldDatabaseNumber, "main.awr.databasenumber");
        properties.setProperty("main.connection.password", passwordFieldDatabasePassword.getText());
    }

    private void makeTextFieldAutoupdateable(JTextField jTextField) {
        jTextField.getDocument().addDocumentListener(
                documentListenerUpdateProperties);
    }

    private void makePropertiesAutoUpdateAble() {
        makeTextFieldAutoupdateable(textFieldDatabaseId);
        makeTextFieldAutoupdateable(textFieldDatabaseNumber);
        makeTextFieldAutoupdateable(textFieldHostName);
        makeTextFieldAutoupdateable(textFieldUsername);
        makeTextFieldAutoupdateable(textFieldBeginSnaphotId);
        makeTextFieldAutoupdateable(textFieldEndShapshotId);
        makeTextFieldAutoupdateable(textFieldDatabaseName);
        makeTextFieldAutoupdateable(textFieldPortNumber);
        passwordFieldDatabasePassword.getDocument().addDocumentListener(
                documentListenerUpdateProperties);
    }

    private void savePropertiesOnClose() {
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                super.windowClosing(e);
                try {
                    properties.store(new FileOutputStream("./connection.properties"),
                            "<!-- No comment -->");
                } catch (IOException error) {
                    System.out.println(error);
                }
            }
        });
    }

    public MainPage(String title) throws HeadlessException {
        super(title);
        loadProperties();
        savePropertiesOnClose();
        connectToDatabaseButton.addActionListener(connectToDB);
        makePropertiesAutoUpdateAble();
        setContentPane(mainpanel);
        buttonLoadAWRReportFromFile.addActionListener(loadAWRReportFromFile);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        buttonShowSnapshots.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Thread(new SnapShotsGetter()).run();
            }
        });
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        JFrame frame = new MainPage("AWR Viewer");
    }
}
