package pl.edu.mimuw.students.jnp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by domandinho on 11.06.15.
 */
public class ChartShower extends JFrame {
    private JPanel panel1;
    private JSlider sliderWhichColumn;
    private JButton buttonMakeChart;
    private Element table;
    private final ArrayList<Class<?>> listOfColumnTypes;

    private Map<String, Double> getPairs() {
        TreeMap<String, Double> result = new TreeMap<>();
        int columnNumber = sliderWhichColumn.getValue();
        Elements rows = table.select("tr");
        for (int i = 1; i < rows.size(); i++) {
            Element currentRow = rows.get(i);
            Elements cells = currentRow.select("td");
            String key = cells.get(0).text();
            Element representativeElement = cells.get(columnNumber);
            String answer = representativeElement.text();
            answer.replace(',', '.');
            double value = Double.parseDouble(answer);
            result.put(key, value);
        }
        return result;
    }

    private ActionListener generateListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            if (listOfColumnTypes.
                    get(sliderWhichColumn.getValue()).equals(String.class)) {
                return;
            } else {
                Map<String, Double> pairs = getPairs();
                Experiments.showChart(pairs);
            }
        }
    };

    public ChartShower(Element table, String statisticName,
                       ArrayList<Class<?>> listOfColumnTypes)
            throws HeadlessException {
        super(statisticName);
        this.listOfColumnTypes = new ArrayList<>();
        for (Class<?> x : listOfColumnTypes) {
            this.listOfColumnTypes.add(x);
        }
        this.table = table;
        sliderWhichColumn.setMaximum(table.select("tr th").size() - 1);
        buttonMakeChart.addActionListener(this.generateListener);
        setContentPane(panel1);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        setVisible(true);
    }
}
