package pl.edu.mimuw.students.jnp;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;

public class AWRVisualisator extends JFrame {
    public final String AWRHtml;
    private JPanel jPanelAWRVisualisator;
    private JTree treeAWRTables;
    private JTable tableStatistic;
    private JLabel labelTableName;
    private JTable tableVisibleColumns;
    private JScrollPane jScrollPaneVisibleColumns;
    private JTextField textFieldFilterRegex;
    private JSlider sliderColumnNumber;
    private JButton buttonVisualizeChart;
    private JButton buttonShowHtml;
    private Document document;
    private DefaultTableModel defaultTableModel;

    private void makeColumnInvisible(String columnName) {
        TableColumn column = tableStatistic.getColumn(columnName);
        column.setMinWidth(0);
        column.setMaxWidth(0);
        column.setPreferredWidth(0);
    }

    private void makeColumnVisible(String columnName) {
        TableColumn column = tableStatistic.getColumn(columnName);
        column.setMinWidth(5);
        column.setMaxWidth(1000);
        column.setPreferredWidth(100);
    }

    private boolean isCurrentElementTypeOf(Element element, String type) {
        return (element.tagName().equals(type));
    }

    private boolean isCurrentElementNamedCorrectlyAndHasProperLevel(Element element,
                                                                    String name, int level) {
        if (element.text().equals(name) == false) {
            return false;
        }
        //0 = any
        if (level != 0) {
            int preferedLevel = level + 1;
            if (element.tagName().equals("h" + preferedLevel) == false) {
                return false;
            }
        }
        return true;
    }


    private Element getTableWhichRepresentsStatistic(String statistic) {
        Elements headersAndTables = document.select("h1, h2, h3, table");
        for (int i = 0; i < headersAndTables.size(); i++) {
            Element currentElement = headersAndTables.get(i);
            if (currentElement.tagName().equals("h3") && currentElement.text().equals(statistic)) {
                //searching for table;
                int j = i + 1;
                while (j < headersAndTables.size()) {
                    Element candidateForBeTable = headersAndTables.get(j);
                    if (candidateForBeTable.tagName().equals("table")) {
                        return candidateForBeTable;
                    } else {
                        return null;
                    }
                }
            }
        }
        return null;
    }

    private ArrayList<Class<?>> listOfColumnClasses;

    private boolean isThisColumnContainOnlyNumbers(Element table,
                                                   int columnNumber) {
        try {
            Elements rows = table.select("tr");
            for (int i = 1; i < rows.size(); i++) {
                Element currentRow = rows.get(i);
                Elements cells = currentRow.select("td");
                Element representativeElement = cells.get(columnNumber);
                try {
                    Double.parseDouble(representativeElement.text());
                } catch (NumberFormatException e) {
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private void updateListOfColumnClasses(Element table) {
        listOfColumnClasses.clear();
        int numberOfColumns = table.select("tr th").size();
        for (int i = 0; i < numberOfColumns; i++) {
            if (isThisColumnContainOnlyNumbers(table, i)) {
                listOfColumnClasses.add(Double.class);
            } else {
                listOfColumnClasses.add(String.class);
            }
        }
    }

    private void findHeaderByNameAndAddHimToTreeStructure(
            DefaultMutableTreeNode father, String nameOfSon, int currentLevel) {
        Elements allElements = document.select("h1, h2, h3, ul");
        for (int iteratorAllElementsInAwrReport = 0; iteratorAllElementsInAwrReport < allElements.size(); iteratorAllElementsInAwrReport++) {
            Element headerElement = allElements.get(iteratorAllElementsInAwrReport);
            if (isCurrentElementNamedCorrectlyAndHasProperLevel(headerElement, nameOfSon, currentLevel)) {
                DefaultMutableTreeNode currentHeaderNode =
                        new DefaultMutableTreeNode(
                                headerElement.text());
                findAndAppendAllSonsToThisNode(nameOfSon, allElements, iteratorAllElementsInAwrReport, headerElement, currentHeaderNode);
                father.add(currentHeaderNode);
                break;
            }
        }
    }

    private void findAndAppendAllSonsToThisNode(String nameOfSon, Elements allElements, int iteratorAllElementsInAwrReport, Element headerElement, DefaultMutableTreeNode currentHeaderNode) {
        int iteratorListSearcher = iteratorAllElementsInAwrReport + 1;
        //Searching for list
        while (iteratorListSearcher < allElements.size()) {
            Element candidateForUnorderedList = allElements.get(iteratorListSearcher);
            if (isCurrentElementTypeOf(candidateForUnorderedList, "h1") ||
                    isCurrentElementTypeOf(candidateForUnorderedList, "h2") ||
                    isCurrentElementTypeOf(candidateForUnorderedList, "h3")) {
                break;
            }
            if (isCurrentElementTypeOf(candidateForUnorderedList, "ul")) {
                Elements allLinksInList = candidateForUnorderedList.select("li a");
                for (int iteratorListElements = 0; iteratorListElements < allLinksInList.size(); iteratorListElements++) {
                    int levelOfSon = 0;
                    if (nameOfSon.equals("Main Report") == false) {
                        levelOfSon = Integer.parseInt(headerElement.tagName().charAt(1) + "");
                    }
                    findHeaderByNameAndAddHimToTreeStructure(currentHeaderNode,
                            allLinksInList.get(iteratorListElements).text(), levelOfSon);
                }
                break;
            } else {
                iteratorListSearcher++;
            }
        }
    }

    private void insertTablesToJTree() {
        DefaultTreeModel model = (DefaultTreeModel) treeAWRTables.getModel();
        DefaultMutableTreeNode oldRoot = (DefaultMutableTreeNode) model.getRoot();
        oldRoot.removeAllChildren();
        DefaultMutableTreeNode root = new DefaultMutableTreeNode("AWR REPORT");
        findHeaderByNameAndAddHimToTreeStructure(root, "Main Report", 0);
        model.setRoot(root);
        model.reload(root);

    }

    private void clearTable() {
        defaultTableModel.setRowCount(0);
        defaultTableModel.setColumnCount(0);
    }

    private void insertColumns(Element table) {
        Elements columnNames = table.select("tr th");
        sliderColumnNumber.setMaximum(columnNames.size() - 1);
        for (int i = 0; i < columnNames.size(); i++) {
            Element columnName = columnNames.get(i);
            defaultTableModel.addColumn(columnName.text());
        }
    }

    private void insertRows(Element table) {
        Elements rows = table.select("tr");
        for (int i = 1; i < rows.size(); i++) {
            Element currentRow = rows.get(i);
            Elements cells = currentRow.select("td");
            ArrayList<Object> allCells = new ArrayList<>();
            for (int j = 0; j < cells.size(); j++) {
                Element currentCell = cells.get(j);
                allCells.add(currentCell.text());
            }
            defaultTableModel.addRow(allCells.toArray());
        }
    }

    private Comparator<String> doubleComparator = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            try {
                o1.replace(',', '.');
                o2.replace(',', '.');
                double d1 = Double.parseDouble(o1);
                double d2 = Double.parseDouble(o2);
                if (d1 < d2) {
                    return -1;
                } else {
                    if (d1 > d2) {
                        return 1;
                    }
                    return 0;
                }
            } catch (NumberFormatException e) {
                return o1.compareTo(o2);
            }
        }
    };

    private Comparator<String> stringComparator = new Comparator<String>() {
        @Override
        public int compare(String o1, String o2) {
            return o1.compareTo(o2);
        }
    };

    private void visualiseTable(Element table, String tableName) {
        tableStatistic.setVisible(false);
        labelTableName.setText(tableName);
        clearTable();
        updateListOfColumnClasses(table);
        insertColumns(table);
        insertRows(table);
        updateTableOfColumns(table);
        specifyComparator();
        tableStatistic.setVisible(true);
    }

    private void specifyComparator() {
        DefaultRowSorter<DefaultTableModel, String> sorter =
                (DefaultRowSorter<DefaultTableModel, String>)
                        (tableStatistic.getRowSorter());
        for (int i = 0; i < listOfColumnClasses.size(); i++) {
            if (listOfColumnClasses.get(i).equals(Double.class)) {
                sorter.setComparator(i, doubleComparator);
            } else {
                sorter.setComparator(i, stringComparator);
            }
        }
    }

    private void showEmptyTable(String tableName) {
        clearTable();
        labelTableName.setText("EMPTY TABLE: " + tableName);
        DefaultTableModel model = (DefaultTableModel) (tableVisibleColumns.getModel());
        model.setRowCount(0);
    }

    private void addListenerToJTree() {
        treeAWRTables.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                TreePath tp = treeAWRTables.getPathForLocation(
                        me.getX(), me.getY());
                if (tp != null) {
                    DefaultMutableTreeNode node = (DefaultMutableTreeNode) (tp.getLastPathComponent());
                    if (node.isLeaf()) {
                        Element element = getTableWhichRepresentsStatistic(node.toString());
                        if (element == null) {
                            showEmptyTable(node.toString());
                            return;
                        }
                        visualiseTable(element, node.toString());
                    }
                }
            }
        });
    }

    private void setVisibleColumnsTableModel() {
        final DefaultTableModel model = new DefaultTableModel() {
            @Override
            public Class<?> getColumnClass(int columnIndex) {
                if (columnIndex == 0) {
                    return String.class;
                } else {
                    return Boolean.class;
                }
            }

            @Override
            public boolean isCellEditable(int row, int column) {
                if (column == 0) {
                    return false;
                } else {
                    return true;
                }
            }

        };
        model.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                for (int i = 0; i < model.getRowCount(); i++) {
                    String columnName = (String) (model.getValueAt(i, 0));
                    boolean columnVisibility = (boolean) (model.getValueAt(i, 1));
                    if (columnName.equals("")) {
                        continue;
                    }
                    if (columnVisibility) {
                        AWRVisualisator.this.makeColumnVisible(columnName);
                    } else {
                        AWRVisualisator.this.makeColumnInvisible(columnName);
                    }
                }
            }
        });
        model.addColumn("Column Name");
        model.addColumn("Visibility");
        tableVisibleColumns.setModel(model);
    }

    private void updateTableOfColumns(Element table) {
        DefaultTableModel columnsModel = (DefaultTableModel) tableVisibleColumns.getModel();
        columnsModel.setRowCount(0);
        Elements columnNames = table.select("tr th");
        for (int i = 0; i < columnNames.size(); i++) {
            Element columnName = columnNames.get(i);
            columnsModel.addRow(new Object[]{columnName.text(), Boolean.TRUE});
        }
    }

    private void filterTables() {
        RowFilter<DefaultTableModel, String> rf = null;
        //If current expression doesn't parse, don't update.
        try {
            String regex = textFieldFilterRegex.getText();
            int columnNumber = sliderColumnNumber.getValue();
            rf = RowFilter.regexFilter(regex, columnNumber);
        } catch (java.util.regex.PatternSyntaxException e) {
            return;
        } catch (NumberFormatException e) {
            return;
        }
        DefaultRowSorter<DefaultTableModel, String> rowSorter =
                (DefaultRowSorter<DefaultTableModel, String>)
                        (tableStatistic.getRowSorter());
        rowSorter.setRowFilter(rf);
    }

    private void implementFilter() {
        tableStatistic.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        DocumentListener filterActionListener = new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterTables();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterTables();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterTables();
            }
        };
        textFieldFilterRegex.getDocument().addDocumentListener(filterActionListener);
    }

    private JEditorPane createEditorPane(String text) {
        try {
            new FileOutputStream("./result.html").write(text.getBytes());
        } catch (Exception e) {
            System.err.println(e);
        }
        JEditorPane editorPane = new JEditorPane();
        editorPane.setEditable(false);
        try {
            editorPane.setPage("file://" + System.getProperty("user.dir") + "/result.html");
            File file = new File("./result.html");
            file.delete();
        } catch (Exception e) {
            System.err.println(e);
        }
        return editorPane;
    }

    private void showHTML() {
        JEditorPane pane = createEditorPane(AWRHtml);
        JFrame frame = new JFrame("AWR Viewer - Pure HTML");
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        JScrollPane jScrollPane = new JScrollPane(pane);
        frame.add(jScrollPane);
        frame.pack();
        frame.setSize(300, 300);
        frame.setVisible(true);
    }

    private ActionListener showHTMLButtonListener = new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    showHTML();
                }
            }).run();
        }
    };

    public AWRVisualisator(String title, String AWRHtml) throws HeadlessException {
        super(title);
        this.AWRHtml = AWRHtml;
        listOfColumnClasses = new ArrayList<>();
        this.defaultTableModel = (DefaultTableModel) tableStatistic.getModel();
        setContentPane(jPanelAWRVisualisator);
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        pack();
        document = Jsoup.parse(AWRHtml);
        tableStatistic.setDefaultEditor(Object.class, null);
        tableStatistic.setAutoCreateRowSorter(true);
        this.buttonVisualizeChart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (labelTableName.getText().substring(0, 11).equals("EMPTY TABLE")) {
                    return;
                }
                if (labelTableName.getText().equals("No data were selected")) {
                    return;
                }
                new ChartShower(getTableWhichRepresentsStatistic(labelTableName.getText()), labelTableName.getText(), listOfColumnClasses);
            }
        });
        buttonShowHtml.addActionListener(showHTMLButtonListener);
        insertTablesToJTree();
        addListenerToJTree();
        setVisibleColumnsTableModel();
        implementFilter();
        setVisible(true);
    }

}
