package pl.edu.mimuw.students.jnp;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;


import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;

class BarChart extends ApplicationFrame {

    public BarChart(String applicationTitle, String chartTitle,
                    Map<String, Double> map) {
        super(applicationTitle);
        JFreeChart barChart = ChartFactory.createBarChart(
                chartTitle,
                "Category",
                "Score",
                createDataset(map),
                PlotOrientation.VERTICAL,
                true, true, false);
        ChartPanel chartPanel = new ChartPanel(barChart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 367));
        setContentPane(chartPanel);
    }


    private CategoryDataset createDataset(Map<String, Double> map) {
        final DefaultCategoryDataset dataset =
                new DefaultCategoryDataset();
        Iterator<Map.Entry<String, Double>> iterator = map.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<String, Double> entry = iterator.next();
            dataset.addValue(entry.getValue(), entry.getKey(), "1");
        }

        return dataset;
    }

}

public class Experiments {
    public static void showChart(Map<String, Double> map) {
        BarChart chart = new BarChart("Chart",
                "Visualisation", map);
        chart.pack();
        RefineryUtilities.centerFrameOnScreen(chart);
        chart.setVisible(true);
    }
}
